package com.example.viewmodel.data.rest;

import com.example.viewmodel.data.model.Repo;
import com.example.viewmodel.event.Result;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import kotlinx.coroutines.experimental.Deferred;
import retrofit2.Call;
import retrofit2.Response;

public class RepoService {

    private final OpenRepoApi mOpenRepoApi;

    @Inject
    public RepoService(OpenRepoApi mOpenRepoApi) {
        this.mOpenRepoApi = mOpenRepoApi;
    }

    // RXjava 2
    /*public Single<List<Repo>> getRepositories() {
        return mOpenRepoApi.getRepositories();
    }*/

    public Single<Repo> getRepo(String owner, String name) {
        return mOpenRepoApi.getRepo(owner, name);
    }

    // Retrofit 2
    public Call<List<Repo>> getRepositories() {
        return mOpenRepoApi.getRepositories();
    }


    // Retrofit 2
    public Deferred<Response<List<Repo>>> getRepositoriesDef() {
        return mOpenRepoApi.getRepositoriesDef();
    }



}
