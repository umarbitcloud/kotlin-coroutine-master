package com.example.viewmodel.data.rest;

import com.example.viewmodel.data.model.Repo;
import com.example.viewmodel.event.Result;

import java.util.List;

import io.reactivex.Single;
import kotlinx.coroutines.experimental.Deferred;
import retrofit2.Response;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface OpenRepoApi {

    //Retrofit 2
    @GET("orgs/Google/repos")
    Call<List<Repo>> getRepositories();

    //Deferred
    @GET("orgs/Google/repos")
    Deferred<Response<List<Repo>>> getRepositoriesDef();


    //Rxjava 2
    /*@GET("orgs/Google/repos")
    Single<List<Repo>> getRepositories();
    */

    @GET("repos/{owner}/{name}")
    Single<Repo> getRepo(@Path("owner") String owner, @Path("name") String name);
}
