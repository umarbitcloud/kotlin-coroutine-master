package com.example.viewmodel.repository

import com.example.viewmodel.data.model.Repo
import com.example.viewmodel.data.rest.RepoService
import com.example.viewmodel.event.PlayErrorEvent
import com.example.viewmodel.event.PlaySuccessEvent
import com.example.viewmodel.event.Result
import com.example.viewmodel.loader.PlayLoader

import javax.inject.Inject

import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.experimental.Deferred
import retrofit2.Response
import java.io.IOException
import kotlin.coroutines.experimental.suspendCoroutine

class MainRepository @Inject
constructor(private val mPlayLoader: PlayLoader) {

    private var mEmpty: String = ""

    /*
    fun getRepositories() {
        mPlayLoader.requestData(mEmpty)
    }*/

    suspend fun getRepositories(): Result<List<Repo>> {
        return mPlayLoader.requestDataCoRoutine(mEmpty)
    }


    fun getRepositoriesDef(): Deferred<Response<List<Repo>>> {
        return mPlayLoader.getRepositoriesDef()
    }


    fun clearPlayLoader() {
        mPlayLoader.clearObserver()
    }

}
