package com.example.viewmodel.loader

import com.example.viewmodel.data.model.Repo
import com.example.viewmodel.data.rest.RepoService
import com.example.viewmodel.event.Result
import io.reactivex.disposables.CompositeDisposable
import kotlinx.coroutines.experimental.Deferred
import org.greenrobot.eventbus.EventBus
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import javax.inject.Inject
import kotlin.coroutines.experimental.suspendCoroutine

class PlayLoader @Inject
constructor(private val mEventBus: EventBus, private val mRepoService: RepoService) : CommonLoader<String> {
    private var mCompositeDisposable: CompositeDisposable? = null


    init {
        this.mCompositeDisposable = CompositeDisposable()
    }


    override fun requestData(data: String) {
    }


    suspend fun requestDataCoRoutine(data: String): Result<List<Repo>> {

        return suspendCoroutine { cont ->
            // RXjava 2
            /*mCompositeDisposable!!.add(mRepoService.repositories.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread()).subscribeWith(object : DisposableSingleObserver<List<Repo>>() {
                        override fun onSuccess(value: List<Repo>) {
                            cont.resume(Result.Success(value!!))
                        }

                        override fun onError(e: Throwable) {
                            cont.resume(Result.Error(IOException(e)))
                        }
                    }))*/

            // retrofit 2 Enqueue
            mRepoService.repositories.enqueue(object : Callback<List<Repo>> {
                override fun onResponse(call: Call<List<Repo>>, response: Response<List<Repo>>) {
                    cont.resume(Result.Success(response.body()!!))
                }

                override fun onFailure(call: Call<List<Repo>>, t: Throwable) {
                    cont.resume(Result.Error(IOException(t)))
                }
            })
        }

    }


    fun getRepositoriesDef(): Deferred<Response<List<Repo>>> {
        return mRepoService.getRepositoriesDef()
    }

    override fun clearObserver() {
        mCompositeDisposable?.clear()
    }

}
