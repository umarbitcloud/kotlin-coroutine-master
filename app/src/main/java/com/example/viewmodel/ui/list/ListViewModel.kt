package com.example.viewmodel.ui.list

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.example.viewmodel.data.model.Repo
import com.example.viewmodel.event.Result
import com.example.viewmodel.repository.MainRepository
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.launch
import javax.inject.Inject

class ListViewModel @Inject
constructor(private val mMainRepository: MainRepository) : ViewModel() {

    private var reposJob: Job? = null

    private val repos = MutableLiveData<List<Repo>>()
    private val repoLoadError = MutableLiveData<Boolean>()
    private val loading = MutableLiveData<Boolean>()

    val error: LiveData<Boolean>
        get() = repoLoadError

    init {
        fetchRepos()
    }

    fun getRepos(): LiveData<List<Repo>> {
        return repos
    }

    fun getLoading(): LiveData<Boolean> {
        return loading
    }

    private fun fetchRepos() {
        loading.value = true

        /* Plain coroutine
        reposJob = launch {

            val value = mMainRepository.getRepositories()
            when (value) {
                is Result.Success -> {
                    repos.postValue(value.data)
                    repoLoadError.postValue(false)
                    loading.postValue(false)
                }
                is Result.Error -> {
                    repoLoadError.postValue(true)
                    loading.postValue(false)
                }
            }

        }*/

        //Deferred coroutine JackWarthon
        reposJob = launch {

            val request = mMainRepository.getRepositoriesDef()
            val value = request.await()

            if (value.isSuccessful) {
                repos.postValue(value.body())
                repoLoadError.postValue(false)
                loading.postValue(false)
            } else {
                repoLoadError.postValue(true)
                loading.postValue(false)
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        mMainRepository.clearPlayLoader()
        reposJob?.cancel()
    }
}

