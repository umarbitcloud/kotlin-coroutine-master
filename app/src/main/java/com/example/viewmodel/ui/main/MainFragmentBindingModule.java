package com.example.viewmodel.ui.main;

import com.example.viewmodel.ui.list.ListFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import com.example.viewmodel.ui.detail.DetailsFragment;

@Module
public abstract class MainFragmentBindingModule {

    @ContributesAndroidInjector
    abstract ListFragment provideListFragment();

    @ContributesAndroidInjector
    abstract DetailsFragment provideDetailsFragment();
}
